jQuery(document).ready(function(e){
  var podplayPlayers = jQuery('.podplay-player');
  var speeds = [ 1, 1.5, 2, 2.5, 3 ];
  var index = 0
  podplayPlayers.each(function(){
    console.log(index);
    index++;
    addPlayerFunc(this);
  })
  
  function addPlayerFunc(player) {
    console.log('wwwww')

    var audio = player.querySelector('audio');
    var play = player.querySelector('.podplay-play');
    var pause = player.querySelector('.podplay-pause');
    var rewind = player.querySelector('.podplay-rewind');
    var progress = player.querySelector('.podplay-progress');
    var speed = player.querySelector('.podplay-speed');
    var mute = player.querySelector('.podplay-mute');
    var currentTime = player.querySelector('.podplay-currenttime');
    var duration = player.querySelector('.podplay-duration');
    var rss = player.querySelector('.podplay-rss');
    var download = player.querySelector('.podplay-download');
    var iTunes = player.querySelector('.podplay-itunes');
    
    var currentSpeedIdx = 0;

    pause.style.display = 'none';
    
    var toHHMMSS = function ( totalsecs ) {
        var sec_num = parseInt(totalsecs, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours; }
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        
        var time = hours+':'+minutes+':'+seconds;
        return time;
    }
    
    audio.addEventListener('loadedmetadata', function(){
      progress.setAttribute('max', Math.floor(audio.duration));
      duration.textContent  = toHHMMSS(audio.duration);
    });
    
    audio.addEventListener('timeupdate', function(){
      progress.setAttribute('value', audio.currentTime);
      currentTime.textContent  = toHHMMSS(audio.currentTime);
    });
    
    play.addEventListener('click', function(event){
      event.preventDefault();
      this.style.display = 'none';
      pause.style.display = 'inline-block';
      pause.focus();
      audio.play();
    }, false);

    pause.addEventListener('click', function(event){
      event.preventDefault();
      this.style.display = 'none';
      play.style.display = 'inline-block';
      play.focus();
      audio.pause();
    }, false);
 
    rewind.addEventListener('click', function(event){
      event.preventDefault();
      audio.currentTime -= 30;
    }, false);
    
    progress.addEventListener('click', function(e){
      event.preventDefault();
      audio.currentTime = Math.floor(audio.duration) * (e.offsetX / e.target.offsetWidth);
    }, false);

    speed.addEventListener('click', function(event){
      console.log('speed')
      event.preventDefault();
      currentSpeedIdx = currentSpeedIdx + 1 < speeds.length ? currentSpeedIdx + 1 : 0;
      audio.playbackRate = speeds[currentSpeedIdx];
      this.textContent  = speeds[currentSpeedIdx] + 'x';
      return true;
    }, false);

    mute.addEventListener('click', function(event) {
      console.log('mute')
      event.preventDefault();
      if(audio.muted) {
        audio.muted = false;
        this.querySelector('.fa').classList.remove('fa-volume-off');
        this.querySelector('.fa').classList.add('fa-volume-up');
      } else {
        audio.muted = true;
        this.querySelector('.fa').classList.remove('fa-volume-up');
        this.querySelector('.fa').classList.add('fa-volume-off');
      }
    }, false);

    rss.addEventListener('click', function(event) {
      event.preventDefault();
      var url = "http://wpcarey.libsyn.com/rss"
      window.open(url);
    });

    download.addEventListener('click', function(event) {
      event.preventDefault();
      var url = jQuery('.podplay-player audio').attr('src');
      window.open(url);
    });

    iTunes.addEventListener('click', function(event) {
      event.preventDefault();
      var url = "https://itunes.apple.com/us/podcast/research-and-ideas-ipod/id203668464?mt=2";
      window.open(url);
    });
  }
});