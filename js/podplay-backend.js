jQuery(document).ready(function(e){

  jQuery('#add_more').click(function(event){
    event.preventDefault();
    podcastCounter = Number(jQuery('input[name="podcasts_counter"').val());
    if(isNaN(podcastCounter)){
      podcastCounter = 0;
    }
    podcastCounter++;
    jQuery('#podcasts_counter').val(podcastCounter);
    cleanNew(podcastCounter);
  });

  function cleanNew(id){
    jQuery('#podplay_metabox_id .podplay_podcast').append(
      '<div class="podcast_section" id="podcast_' + id + '" style="border-bottom: 1px solid #cecece; padding: 0 10px 10px; margin-bottom: 10px;"><p>Shortcode: <code>[podcast id="' + id + '"]</code></p>'
      + '<label for="podcast_url">'
      + 'Podcast URL:'
      + '<input type="url" name="podcast_url[' + id + ']" id="podcast_url" style="max-width: 560px; width: 100%; margin-left: 10px; margin-bottom: 10px;" value=""/>'
      + '</label> <button class="button deletepodcast button-small">Delete</button> <button class="button savepodcast button-small">Update</button>'
      );
  }

  //Validate URLs before they get submitted
  function validateURL(textval) {
    var urlregex = new RegExp(
          "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
    return urlregex.test(textval);
  }
  jQuery(document).on('click', '.button.deletepodcast', function(){ 
    // Your Code
    event.preventDefault();
    jQuery(this).parent().remove();
    jQuery('#podcasts_update').removeAttr("disabled");
  });

  jQuery('#podplay_metabox_id').on('change', 'input[type="url"]', function(){
    jQuery('#podcasts_update').removeAttr("disabled");
  });

  jQuery('button.savepodcast').on('click', function(event) {
    var goodSumbit = true;
    var allURLS = jQuery('#podplay_metabox_id input[type="url"]');
    var allURLSLength = allURLS.length - 1;
    allURLS.each(function(index, value){
      if(validateURL(jQuery(this).val())){
        //do nothing
      }else{
        goodSumbit = false;
        jQuery(this).parent().prepend('<div class="error">Invalid URL</div>');
      }
      if(allURLSLength == index && !goodSumbit){
          event.preventDefault();
      }
    })
  });

});