<div class="podplay-player" id="podcast-<?php echo $podcast_id ?>">
    <link rel="stylesheet" href="<?php echo plugins_url( 'css/podplay-player.css', __FILE__ );?>" type="text/css" media="all">
    <div class="podplay-player-controls">
        <button class="podplay-play"><i class="fa fa-play"></i><span class="text">Play</span></button>
        <button class="podplay-pause"><i class="fa fa-pause"></i><span class="text">Pause</span></button>
        <button class="podplay-rewind"><i class="fa fa-fast-backward"></i><span class="text">Rewind</span></button>
        <div class="podplay-progress-section">
            <span class="podplay-currenttime podplay-time">00:00</span>
            <progress class="podplay-progress" value="0"></progress>
            <span class="podplay-duration podplay-time">00:00</span>
        </div>
        <button class="podplay-speed">1x</button>
        <button class="podplay-mute"><i class="fa fa-volume-up"></i><span class="text">Mute/Unmute</span></button>
        <button class="podplay-download"><i class="fa fa-download"></i><span class="text">Download</span></button>
        <button class="podplay-rss"><i class="fa fa-rss"></i><span class="text">RSS</span></button>
        <button class="podplay-itunes"><i class="fa fa-music"></i><span class="text">iTunes</span></button>
    </div>
    <audio src="<?php echo $podcast ?>"></audio>
</div>
