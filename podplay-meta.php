<?php
/*
Plugin Name: PodPlay - HTML5 Podcast Player
Description: Add a podcast player to post.
Version: 1.4
Author: Danny Martinez
Author URI: http://wpcarey.asu.edu

Bitbucket Plugin URI: https://bitbucket.org/dmarti18/podplay
Bitbucket Branch:     master
*/

/*
 * Adds a box to the main column on the Post and Page edit screens.
 */

function podplay_add_meta_box() {
	$screens = array( 'post', 'page' );
	foreach ( $screens as $screen ) {
		add_meta_box(
			'podplay_metabox_id',
			__( 'Podcasts', 'podplay_plugin' ),
			'podplay_metabox_callback',
			$screen
		);
	}
}

add_action( 'add_meta_boxes', 'podplay_add_meta_box' );

function podplay_metabox_callback( $post ) {

	// add an nonce field so we can check for it later.
	wp_nonce_field( 'podplay_meta_box', 'podplay_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	wp_enqueue_script(
		'podplayjs_backend',
		plugins_url( 'js/podplay-backend.js', __FILE__ )
	);

	wp_enqueue_script(
		'podplayjs_player',
		plugins_url( 'js/podplay-player.js', __FILE__ )
	);

	$podplay_url = get_post_meta( $post->ID, '_podcast_url', true );
	$podplay_counter = get_post_meta( $post->ID, '_podcast_counter', true );

	if(empty($podplay_counter)){
		$podplay_counter = 0;
	}

	//legacy support for older version of podplay
	if( !is_array($podplay_url) && !empty($podplay_url) ){
		$podplay_url = array(
			1 => $podplay_url,
		);
		$podplay_counter = 1;
	};

	echo '<div class="podplay_podcast">';
	echo '<input type="hidden" name="podcasts_counter" value="'. $podplay_counter .'" id="podcasts_counter">';
	if( !empty($podplay_url) ){
		foreach ($podplay_url as $podcast_id => $podcast) {
			// Display the form, using the current value.
			echo '<div class="podcast_section" id="podcast_'. $podcast_id .'" style="border-bottom: 1px solid #cecece; padding: 0 10px 10px; margin-bottom: 10px;"><p>Shortcode: <code>[podcast id="' . $podcast_id . '"]</code></p>';
			echo '<label for="podcast_url">';
			echo 'Podcast URL:';
			echo '<input type="url" name="podcast_url[' . $podcast_id . ']" id="podcast_url" style="max-width: 560px; width: 100%; margin-left: 10px; margin-bottom: 10px;" value="';
			echo esc_attr($podcast);
			echo '"/>';
			echo '</label> <button class="button deletepodcast button-small">Delete</button> <button class="button savepodcast button-small">Update</button>';
			if (!empty($podcast)) {
				$podcast = $podcast;
				echo '<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">';
				include('podplay-player.php');
			}
			echo '</div>';
		}
	}
	echo '</div>';
	echo '<button id="add_more">Add</button> <button id="podcasts_update" disabled="disabled">Update</button>';

}

function player_creation($atts){
	$podcast_player_data = get_post_meta(get_the_ID(), '_podcast_url', true);
	$shortcode_input = shortcode_atts( array(
							'id' => '',
						), $atts );
    $podcast_id = $shortcode_input['id'];

	//does shortcode have an id and the podcast data isn't an array
	if(empty($podcast_id) && !is_array($podcast_player_data) ){
		//make $podcast_id equal zero and convert string url to an array to match when it normally is an array 
		$podcast_id = 1;
		$podcast_player_data[$podcast_id] = $podcast_player_data;
	}elseif (empty($podcast_id)) {
		// grab the first podcast in the array
		$podcast_id = current(array_keys($podcast_player_data));
	}

	$podcast = $podcast_player_data[$podcast_id];

	if (!empty($podcast)) {
		wp_enqueue_script(
			'podplayjs',
			plugins_url( 'js/podplay-player.js', __FILE__ )
		);
		ob_start();
		include 'podplay-player.php';
		return ob_get_clean();
	}
}

add_shortcode('podcast', 'player_creation');

function podplay_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['podplay_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['podplay_meta_box_nonce'], 'podplay_meta_box' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset( $_POST['podcast_url'] ) ) {
		$podplay_url_data = [];
	}

	// Sanitize user input.
	if(!empty($_POST['podcast_url'])){
		foreach ($_POST['podcast_url'] as $key => $value) {
			$podcast_url_sanitized[$key] = esc_url($value);
		} 
	}

	$podplay_counter_data = $_POST['podcasts_counter'];

	// Update the meta field in the database.
	update_post_meta( $post_id, '_podcast_counter', $podplay_counter_data );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_podcast_url', $podcast_url_sanitized );
}


add_action( 'save_post', 'podplay_save_meta_box_data' );
